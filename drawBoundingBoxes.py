import cv2
import csv


def drawBoxes(boxPath, imgPath):
    f = open(boxPath, 'r')
    csvreader = csv.reader(f);

    color = 0
    for row in csvreader:
        print(row)
        img = cv2.imread(imgPath)
        cv2.rectangle(img, (int(float(row[1])), int(float(row[2]))), (int(float(row[3])), int(float(row[4]))), (155, 52, 235), 4)
        cv2.imshow(imgPath, img)
        cv2.waitKey(0)
        cv2.destroyAllWindows()

    return;

#drawBoxes('./test_objects/0/1/0_bounding_boxes.txt', './test_objects/0/1/0_rgb.png')
