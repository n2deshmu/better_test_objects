import json
import os
import cv2
import csv

x_test = []
y_test = []

#last_image =0

def loadXImages(participantNumber, sessionNumber):
    # if x < 1:
    #     return
    
    dictPath = 'learned_objects\\' + str(participantNumber) + '\\object_label_back_dictionary.json'
    data = json.load(open(dictPath))
    imgsFromSessionDirectory = 'test_objects\\' + str(participantNumber) + '\\' + str(sessionNumber)
    for dirpath, dirnames, filenames in os.walk(imgsFromSessionDirectory):
        for filename in filenames:
            if filename.endswith('rgb.png'):

                imgPath = os.path.join(dirpath, filename)
                boxPath = os.path.join(dirpath, filename.split('_')[0] + '_bounding_boxes.txt')

                imgFile = open(imgPath)
                boxFile = open(boxPath)

                boxes = csv.reader(boxFile)
                img = cv2.imread(imgPath)
                if boxes != []:
                    #i = 0
                    #img_count =0
                    # and (i<x) and (img_count >= last_image)
                    for box in boxes:
                        cropped_image = img[int(float(box[2])):int(float(box[2]))+int(float(box[4])), int(float(box[1])):int(float(box[1]))+int(float(box[3]))]
                        if(cropped_image.size > 0) and (box[0] in data):
                            #print(imgPath)
                            #cv2.imwrite(imgPath, cropped_image)
                            x_test.append(cropped_image)
                            y_test.append(box[0])
                            #i = i+1
                            print(box, imgPath)
                        #img_count = img_count + 1
                    
                    #last_image = i
                
    


    return
