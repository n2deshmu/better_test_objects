import os
import time

from drawBoundingBoxes import drawBoxes

def iterateOverDirectory(directory):
    dictPathOld = ''
    boxPathOld = ''
    for dirpath, dirnames, filenames in os.walk(directory):
        for filename in filenames:
            if filename.endswith('rgb.png'):

                imgPath = os.path.join(dirpath, filename)
                boxPath = os.path.join(dirpath, filename.split('_')[0] + '_bounding_boxes.txt')
                dictPath = 'learned_objects\\' + imgPath.split('\\')[1] + '\\object_label_dictionary.json'
                
                print(boxPath, imgPath, dictPath)

                if dictPath != dictPathOld:
                    openNotepad(dictPath)
                    dictPathOld = dictPath
                
                if boxPath != boxPathOld:
                    
                    if not os.path.exists('better_'+ dirpath):
                        os.makedirs('better_'+ dirpath)
                        os.makedirs('better1_'+ dirpath)

                    if not os.path.exists('better1_'+ dirpath):
                        os.makedirs('better1_'+ dirpath)
                    
                    if not os.path.isfile('better_' + boxPath):
                        newBoundingBoxFile = open('better_' + boxPath, 'w')
                        oldBoundingBoxFile = open(boxPath, 'r')
                        oldBoundingBox = oldBoundingBoxFile.read()
                        newBoundingBoxFile.write(oldBoundingBox)
                        newBoundingBoxFile.close()
                        oldBoundingBoxFile.close()
                        newSimplerFile = open('better1_' + boxPath, 'w+')
                        newSimplerFile.close()

                    openNotepad('better_' + boxPath)
                    time.sleep(1)
                    openNotepad('better1_' + boxPath)
                    boxPathOld = boxPath

                drawBoxes(boxPath, imgPath)
    return


def openNotepad(dictPath):
    os.system('start '+ dictPath)
    return


iterateOverDirectory('test_objects\\8\\0')