import json

dictionary = "genericLabelsDict.json"

forwardDict = json.load(open(dictionary))

backDict = {}

for x in forwardDict:
    for item in forwardDict[x]:
        backDict[item] = x


print(backDict)

with open("genericLabelsDictBack.json", "w") as outfile:
    json.dump(backDict, outfile, indent = 4)