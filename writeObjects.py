import os
import json

def iterateOverDirectory(directory, dictionary):
    dictPathOld = ''
    boxPathOld = ''
    for dirpath, dirnames, filenames in os.walk(directory):
        for filename in filenames:
            if filename.endswith('.txt'):
                answerPath = os.path.join(dirpath, filename)
                toFix = answerPath.split('\\')
                del toFix[0]
                toFix.insert(0, 'better_test_objects')
                toFixPath = '\\'.join(toFix)
                #print(toFixPath);

                data = json.load(open(dictionary))

                answerFile = open(answerPath, 'r')
                answer = answerFile.readline().split(',')

                toFixFile = open(toFixPath, 'r')
                toFixList = toFixFile.readlines()
                toFixFile.close()

                newCorrectList = [];

                for idx, x in enumerate(toFixList):
                    itemList = x.split(',')
                    del itemList[0]
                    if(answer[idx] == '-'):
                        del x
                    else:
                        itemList.insert(0, data[answer[idx]])
                        newCorrectList.append(','.join(itemList))

                # print(answer)
                # print(toFixList)
                print(newCorrectList)

                toFixFile = open(toFixPath, 'w+')
                toFixFile.writelines(newCorrectList)
                toFixFile.close()
    
                toFixFile.close()
                answerFile.close()

    return

dictionary = "learned_objects\\8\\object_label_dictionary.json"
iterateOverDirectory('better1_test_objects\\8\\0', dictionary)